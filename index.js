const puppeteer = require('puppeteer');

const username = '';
const password = '';


async function loginFlow(page, username, password) {
  // 点击登录按钮
  await page.click('#topbar_loginAndReg > a.iblock.fr.ima.ztag')
  await page.evaluate(() => console.log(`url is haha`));

  // 等待认证的iframe加载出来
  await page.waitFor(4000)
  const frames = await page.frames();
  const loginFrame = frames.find(f => f.name().startsWith('x-URS-iframe'));

  await loginFrame.waitForSelector('input[data-loginname="loginEmail"]')
  await loginFrame.type('input[data-loginname="loginEmail"]', username)
  await loginFrame.type('input.j-inputtext[type="password"]', password)
  await loginFrame.click('#dologin')
  await page.waitFor(1000)
}

async function getBlogList(page) {
  let blog_list = []
  let next_button_flag = 0
  do {
    // 获取博客列表href
    await page.waitForSelector('div.pleft > h3 > a')
    const per_page_blog_list = await page.$$eval('div.pleft > h3 > a', hrefs => hrefs.map((a) => {
      return a.href
    }))
    
    blog_list.push(...per_page_blog_list)  
    // console.log(per_page_blog_list)  

    // 获取下一页的按钮元素
    await page.waitForXPath('//div/span[contains(text(), "下一页")]')
    const next_page_button = await page.$x('//div/span[contains(text(), "下一页")]')
    // 是否还有下一页
    next_button_flag = await page.$$eval('div span.pgi', page_span => {
      return page_span.filter(span => 
        span.textContent == '下一页' &&　window.getComputedStyle(span).getPropertyValue('visibility') !== 'hidden'
      ).length > 0
    })
    await next_page_button[0].click()
  } while(next_button_flag)

  return blog_list
}

async function getTagList(page, blog_url) {
  await page.goto(blog_url, {waitUntil: 'domcontentloaded'})
  // await page.waitForNavigation({ waitUntil: 'networkidle' });
  await page.waitForSelector('div.multicntwrap > div.multicnt > div > p > span.pleft > a', {timeout: 50000})
  const category_list = await page.$$eval('div.multicntwrap > div.multicnt > div > p > span.pleft > a', category_list =>
    category_list.map(category => category.textContent)
  )
  const tag_list = await page.$$eval('[id="$_blogTagInfo"] > a', tag_list =>
    tag_list.map(tag => tag.textContent)
  )
  return {blog_url, category_list, tag_list}
}

async function getBlogDetailsList() {
  const browser = await puppeteer.launch({
    // executablePath: '/usr/bin/google-chrome',
    // headless: false,
    // args: ['--start-fullscreen', '--window-size=1920,1040'],
    // args: ['--start-fullscreen', '--window-position=0,600'],
    // devtools: true,
    // slowMo: 250 // slow down by 250ms
  })
  const page = await browser.newPage()
  
  // page.on('console', msg => console.log('PAGE LOG:', msg.text()));
  
  // 获取博客的id
  let blog_id_result_list = []
  page.on('response', response => {
    if (typeof response.url() === "string" && response.url().endsWith('BlogBeanNew.getBlogs.dwr')) {
      response.text().then(function (data) {
        data.split('.permalink="').slice(1).forEach(text => 
          blog_id_result_list.push({
            blog_url: "http://gaozhidf.blog.163.com/" + text.split('";', 1)[0] + '/', 
            blog_id: text.split('.blogId=')[1].split(';s')[0]}
          )
        )
      })
    }
  });
  await page.setViewport({ width: 1280, height: 768 })
  // await page.setViewport({ width: 1920, height: 1080 })

  await page.goto('http://gaozhidf.blog.163.com/blog/#m=0', {waitUntil: 'domcontentloaded'})
  // await page.goto('http://ran417.blog.163.com/blog/#m=0', {waitUntil: 'domcontentloaded'})
  // 去掉迁移通知
  await page.click('a.iknow.ztag')
  
  // 登录流程
  await loginFlow(page, username, password)
  // 获取博客列表
  const blog_list = await getBlogList(page)

  // console.log(blog_list)

  // 获取分类和标签列表
  let result_list = []
  for (blog_list_index in blog_list) {
    const tmp_obj = await getTagList(page, blog_list[blog_list_index])
    // 加入博客的id
    Object.assign(tmp_obj, blog_id_result_list.find(
      result => result.blog_url == blog_list[blog_list_index]
    ))
    // console.log(tmp_obj)
    result_list.push(tmp_obj)
  }
  // console.log(result_list)

  await browser.close()
  return result_list
}

// export {getBlogDetailsList}
try {
  (async () => {
    const result_list = await getBlogDetailsList()
    console.log(result_list)
    var fs = require('fs');

    fs.writeFile ("input.json", JSON.stringify(result_list), function(err) {
        if (err) throw err;
        console.log('complete');
        }
    );
  })()
} catch (error) {
  
}