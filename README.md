# 163_blog_spider

登录，爬取网易博客，并根据网易博客导出的ｘｍｌ生成hexo blog内容

#### 0. about
代码其实是用来备份网易博客用的，因为用了2013年到2018年，虽然不勤但也有不少博客，虽然只是摘抄比较多，也留下来作纪念吧

#### 1. setup
安装依赖，主要是puppeteer，可以使用`ELECTRON_MIRROR="https://npm.taobao.org/mirrors/electron/" npm  install --verbose electron`安装
```
npm install
```

#### 2. run
1. 先是index.js，利用puppetter去抓取网易博客内容，生成input.json，里面主要是博客id和便签信息
2. 然后是将自己的博客从网易博客到处，有对应的xml内容，将这些内容和input.json去生成hexo博客
```
node index.js
node paste_to_blog.js
```

