var fs = require('fs');
var xml2js = require('xml2js');
var moment = require('moment');
var blog_list_web = require('./input.json')

var parser = new xml2js.Parser();

let blog_head = (title, tags, categories, date_str) => {
  let tag_str = ''
  tags.forEach(tag => {
    tag_str += ' - ' + tag + '\n' 
  });
  let categories_str = ''
  categories.forEach(category => {

    categories_str += ' - ' + category + '\n' 
  });

  return `title: ${title}
author: Zhi Gao
tags:
${tag_str}
categories:
${categories_str}
date: ${date_str}
password: 163
abstract: 密码是163，只是留作记录但不想被爬到
message: 密码是163，只是留作记录但不想被爬到
---
`
}

fs.readFile(__dirname + '/网易博客日志列表.xml', function(err, data) {
    parser.parseString(data, function (err, result) {
        console.log(result.root.blog.length)
        // console.dir(result);
        console.log('Done');

        console.log(blog_list_web.length)

        const result_list = result.root.blog.filter(blog => 
          blog_list_web.filter(blog_web =>
            blog_web.blog_id == blog.id
          ).length == 0
        )
        // 漏掉的blog_id
        console.log(result_list.map(blog => blog.id))
        console.log(result_list.length)
        console.log(result.root.blog.map(blog => blog.id).length)
        console.log([...new Set(result.root.blog.map(blog => blog.id))].length)
        
        result.root.blog.forEach(blog => {
          target_blog_list_web = blog_list_web.filter(blog_web => blog_web.blog_id == blog.id)
          if(target_blog_list_web.length > 1) {
            console.log('i am bigger than 1')
            console.log(target_blog_list_web)
          }
          const tag_list = target_blog_list_web.length>0?target_blog_list_web[0].tag_list:[]

          blog_content = blog_head(
            blog.title, 
            tag_list, 
            [blog.className], 
            // moment(new Date(blog.publishTime)).format('YYYY-MM-DDTHH:mm:ss')
            blog.publishTime
          )
          fs.writeFile('./163_blog/' + blog.id.toString() + '.md', blog_content + blog.content, function(err) {
            if (err) throw err;
            console.log('complete' + blog.id.toString());
          });
        })
        
    });
});


